<?php

namespace App\Service;

use Illuminate\Support\Facades\Cache;

class FeedCovid
{

    private function getContent($url){
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Cookie: __cfduid=dab9d3dee8942865c27e957fdfaeb90a11586046571"
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    }

    public function thansettakij(){ //ตลาดเงิน - ตลาดทุน
        $data = $this->getContent('https://www.thansettakij.com/feed/covid19_news/10');
        Cache::store('file')->add('thansettakij', $data, 600);
        $value = Cache::store('file')->get('thansettakij');
        if($value) $result = json_decode($value, true);
        else $result = json_decode($data, true);
        $arr = [];
        foreach($result as $key => $data){
            $data['topic'] = htmlspecialchars_decode($data['topic']);
            $arr[$key] = $data;
            $arr[$key]['cat'] = 'money';
        }
        return $arr; 
    }

    public function bangkokbiznews(){ //การเงิน-การลงทุน
        $data = $this->getContent('https://www.bangkokbiznews.com/feed/covid19_news/10');
        Cache::store('file')->put('bangkokbiznews', $data , 600);
        $value = Cache::store('file')->get('bangkokbiznews');
        if($value) $result = json_decode($value, true);
        else $result = json_decode($data, true);
        $arr = [];
        foreach($result as $key => $data){
            $data['topic'] = htmlspecialchars_decode($data['topic']);
            $arr[$key] = $data;
            $arr[$key]['cat'] = 'finance';
        }
        return $arr; 
    }

    public function allContent(){
        $array1 = $this->thansettakij();
        $array2 = $this->bangkokbiznews();
        $result = array_merge($array1, $array2);
        $arr = [];
        foreach($result as $data){
            $arr[strtotime($data['publish_date'])] = $data;
        }
        krsort($arr);
        $arr = array_values($arr);
        return $arr;
    }
}
