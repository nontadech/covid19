<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Article;
use Illuminate\Database\Eloquent\SoftDeletes;

class File extends Model
{
    use SoftDeletes;

    protected $table = 'file';

    public function article()
    {
        return $this->belongsTo(Article::class, 'article_id', 'id');
    }
}
