<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Model\File;

class Article extends Model
{
    use SoftDeletes;
    protected $table = 'article';

    public function file()
    {
        return $this->hasOneThrough(File::class, Article::class, 'id', 'article_id');
    }
}
