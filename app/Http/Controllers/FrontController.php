<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use App\Service\FeedCovid;
use App\Model\Article;


class FrontController extends BaseController
{
    public function index(){
        
        $articles = Article::where('categories_id', 1)
        ->orderBy('id', 'desc')
        ->limit(3)
        ->get();
        foreach($articles as $article){
            $article['file'] = $article->file;
            $article['file']['path'] = file_exists( public_path() . '/images/' . $article['file']['path']) ?  url('images/'.$article['file']['path']) : asset($article['file']['path']);
        }
        $data['article_update'] = $articles ?? [];
        unset($articles);
        
        $articles = Article::where('categories_id', 2)->first();
        $data['article_live'] = $articles ?? [];
        unset($articles);
        
        $articles = Article::where('categories_id', 3)
        ->orderBy('id', 'desc')
        ->get();
        foreach($articles as $article){
            $article['file'] = $article->file;
            if($article['file'])
            $article['file']['path'] = file_exists( public_path() . '/images/' . $article['file']['path']) ?  url('images/'.$article['file']['path']) : asset($article['file']['path']);
        }
        $data['article_faq'] = $articles ?? [];
        unset($articles);

        $articles = Article::where('categories_id', 4)
        ->orderBy('id', 'desc')
        ->get();
        foreach($articles as $article){
            $article['vdo'] = $article->file::where('type', 'vdo')->where('article_id', $article->id)->first();
            if($article['vdo'])
                $article['vdo']['path'] = file_exists( public_path() . '/vdo/' . $article['vdo']['path']) ?  url('vdo/'.$article['vdo']['path']) : asset($article['vdo']['path']);
            $article['picture'] = $article->file::where('type', 'picture')->where('article_id', $article->id)->first();
            if($article['picture'])
                $article['picture']['path'] = file_exists( public_path() . '/images/' . $article['picture']['path']) ?  url('images/'.$article['picture']['path']) : asset($article['picture']['path']);
        }
        $data['article_vdo'] = $articles ?? [];
        unset($articles);

        $articles = Article::where('categories_id', 5)->first();
        $articles['file'] = $articles->file;
            if($articles['file'])
            $articles['file']['path'] = file_exists( public_path() . '/images/' . $articles['file']['path']) ?  url('images/'.$articles['file']['path']) : asset($articles['file']['path']);
        $data['article_banner'] = $articles ?? [];
        unset($articles);

        $feedCovid = new FeedCovid();
        $data['content'] = $feedCovid->allContent() ?? [];
        return view('pages.home', $data);
    }

    public function info($id){
        $data['url'] = "assets/images/info_".$id."_detail.jpg"; 
        $data['url_1'] = '';
        if($id == 4){
            $data['url_1'] = "assets/images/info_".$id."-1_detail.jpg";
        }
        return view('pages.info', $data);
    }
    public function infoList(){
        $data = [];
        $detail = [
            'ใครบ้าง หมดสิทธิ์รับ 5,000 บาท เยียวยาโควิด-19',
            '4 อาชีพ เฮ ล็อตแรก 5,000 บาท เยียวยาโควิด-19',
            'กลุ่มเป้าหมายรับสิทธิ์เยียวยา 5,000 บาท (3 เดือนมาจากไหน)',
            'เราไม่ทิ้งกัน ล็อตแรก 5,000 บาท ถึง 1.4 ล้านคน!',
            'ทำไมไม่ได้ 5,000 จัดอยู่กลุ่ม เกษตรกร',
            "ร้องเรียนอย่าไปคลัง ไม่ได้ 5,000 ยื่น 'อุทธรณ์' เฉพาะ online เท่านั้น",
            "เคลียร์ชััด? ได้สิทธิ์ 5,000 แต่เงินไม่เข้าบัญชี",
            "คืบหน้าเยียวยา 5,000 บาท",
            "เปิดเกณฑ์ ทบทวนสิทธิ์ เยี่ยวยา 5,000 บาท"
        ];
        for($x=0; $x<count($detail); $x++){
            $data['info'][$x]['url'] = "assets/images/info_".($x+1)."_detail.jpg"; 
            $data['info'][$x]['detail'] = $detail[$x];
        }
        
        return view('pages.list', $data);
    }
}
