<?php

namespace App\Http\Controllers\Api;

use Illuminate\Routing\Controller as BaseController;

use App\Service\FeedCovid;

class ContentController extends BaseController
{
    public function index($cat){
        $feedCovid = new FeedCovid();
        if($cat == 'money'){
            $data = $feedCovid->thansettakij() ?? [];
        }else if($cat == 'finance'){
            $data = $feedCovid->bangkokbiznews() ?? [];
        }else{
            $data = $feedCovid->allContent() ?? [];
        }
        
        return response()->json($data, 200);;
    }

   
}
