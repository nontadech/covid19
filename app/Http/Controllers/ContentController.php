<?php

namespace App\Http\Controllers;

use App\Model\Article;
use App\Model\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ContentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public $categories_id;
    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function news()
    {
        $this->categories_id = 1;
        return $this->list();
    }

    public function live()
    {
        $data['categories_id'] = 2;
        $article = Article::find(10);
        $data['article'] = $article;
        return view('pages.content.edit', $data);
    }

    public function faq()
    {
        $this->categories_id = 3;
        return $this->list();
    }

    public function vdo()
    {
        $this->categories_id = 4;
        return $this->list();
    }

    public function banner()
    {
        $data['categories_id'] = 5;
        $article = Article::find(31);
        $data['article'] = $article;
        $data['article']['file'] = $article->file::where('type', 'picture')->where('article_id', $article->id)->first();
        if($data['article']['file']) 
            $data['article']['file']['path'] = file_exists( public_path() . '/images/' . $data['article']['file']['path']) ?  url('images/'.$data['article']['file']['path']) : asset($data['article']['file']['path']);
        return view('pages.content.edit', $data);
    }

    public function list()
    {
        $articles = Article::where('categories_id', $this->categories_id)
        ->orderBy('id', 'desc')
        ->get();
        foreach( $articles as $article){
            $article['file'] = $article->file::where('type', 'picture')->where('article_id', $article->id)->first() ? $article->file::where('type', 'picture')->where('article_id', $article->id)->first() : '';
            if($article['file']){
                $article['file']['path'] = file_exists( public_path() . '/images/' . $article['file']['path']) ?  url('images/'.$article['file']['path']) : asset($article['file']['path']);
            }
        }
        $data['article_list'] = $articles ?? [];
        $data['categories_id'] = $this->categories_id;
        return view('pages.content.list', $data);
    }    


    public function create(Request $request)
    {
        $data['categories_id'] = $request->input('categories_id');
        return view('pages.content.create', $data);
    }

    public function store(Request $request)
    {
        $categories_id = $request->input('categories_id');
        if(in_array($categories_id, [1])){
            request()->validate([
                'picture' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'title' => 'required'
            ]);
        }else if(in_array($categories_id, [3])){
            request()->validate([
                'picture' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
            ]);
        }else if(in_array($categories_id, [4])){
            request()->validate([
                'picture' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'vdo' => 'required|mimes:mp4'
            ]);
        }
        $article = new Article();
        $article->title = $request->input('title') ?? '';
        $article->descrition = $request->input('descrition') ?? '';
        $article->categories_id = $request->input('categories_id');
        $article->users_id = Auth::id();
        $article->save();

        $imageName = time().'.'.request()->picture->getClientOriginalExtension();
        request()->picture->move(public_path('images'), $imageName);
        $file = new File();
        $file->article_id = $article->id;
        $file->type = 'picture';
        $file->path = $imageName;
        $file->save();
        if(in_array($categories_id, [4])){
            $vdoName = time().'.'.request()->vdo->getClientOriginalExtension();
            request()->vdo->move(public_path('vdo'), $vdoName);
            $file = new File();
            $file->article_id = $article->id;
            $file->type = 'vdo';
            $file->path = $vdoName;
            $file->save();
        }
        return redirect(route('content.index', ['categories_id' => $request->input('categories_id')]));
    }

    public function show($id)
    {
        //
    }
    
    public function edit(Request $request, $id)
    {
        $data['categories_id'] = $request->input('categories_id');
        $article = Article::find($id);
        $data['article'] = $article;
        $data['article']['file'] = $article->file::where('type', 'picture')->where('article_id', $article->id)->first();
        if($data['article']['file']) 
            $data['article']['file']['path'] = file_exists( public_path() . '/images/' . $data['article']['file']['path']) ?  url('images/'.$data['article']['file']['path']) : asset($data['article']['file']['path']);
       
        $data['article']['vdo'] = $article->file::where('type', 'vdo')->where('article_id', $article->id)->first();
        if($data['article']['vdo'])
            $data['article']['vdo']['path'] = file_exists( public_path() . '/vdo/' . $data['article']['vdo']['path']) ?  url('vdo/'.$data['article']['vdo']['path']) : asset($data['article']['vdo']['path']);
       
        return view('pages.content.edit', $data);
    }
    
    public function update(Request $request, $id)
    {
        $categories_id = $request->input('categories_id');
        $article = Article::find($id);
        $article->title = $request->input('title') ?? '';
        $article->descrition = $request->input('descrition') ?? '';
        $article->categories_id = $request->input('categories_id');
        $article->users_id = Auth::id();
        $article->save();

        if(request()->picture){
            request()->validate([
                'picture' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
            ]);
            $imageName = time().'.'.request()->picture->getClientOriginalExtension();
            request()->picture->move(public_path('images'), $imageName);
            $file = File::firstWhere('article_id', $id);
            $file->article_id = $article->id;
            $file->type = 'picture';
            $file->path = $imageName;
            $file->save();
        }  
        if(in_array($categories_id, [5])){
            $file = File::firstWhere('article_id', $id);
            $file->link = $request->input('link') ?? '';
            $file->save();
        }
        if(request()->vdo){
            request()->validate([
                'vdo' => 'required|mimes:mp4'
            ]);
            $vdoName = time().'.'.request()->vdo->getClientOriginalExtension();
            request()->vdo->move(public_path('vdo'), $vdoName);
            $file = new File();
            $file->article_id = $article->id;
            $file->type = 'vdo';
            $file->path = $vdoName;
            $file->save();
        }
        if(in_array($categories_id, [1, 3, 4])){
            return redirect(route('content.index', ['categories_id' => $request->input('categories_id')]));
        }else{
            if($categories_id == 2){
                return $this->live();
            }else if($categories_id == 5){
                return $this->banner();
            }
         
           // return redirect(route('content.index', ['categories_id' => $request->input('categories_id')]));
        }
        
    }
    
    public function destroy($id)
    {
        $article = Article::find($id);
        if ($article != null) {
            $article->delete();
        }
        $file = File::firstWhere('article_id', $id);
        if ($file != null) {
            $file->delete();
        }
     //   return redirect(route('content.index', ['categories_id' => $request->input('categories_id')]));
    }
    public function index(Request $request)
    {
        $this->categories_id = $request->input('categories_id');
        return $this->list();
    }
}
