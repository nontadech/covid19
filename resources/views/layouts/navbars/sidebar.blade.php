<div class="sidebar" data-color="orange" data-background-color="white" data-image="{{ asset('material') }}/img/sidebar-1.jpg">
  <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
  <div class="logo">
    <a href="https://creative-tim.com/" class="simple-text logo-normal">
      {{ __('Creative Tim') }}
    </a>
  </div>
  <div class="sidebar-wrapper">
    <ul class="nav">
      <li class="nav-item {{ ($activePage == 'profile' || $activePage == 'user-management') ? ' active' : '' }}">
        <a class="nav-link" data-toggle="collapse" href="#laravelExample" aria-expanded="true">
        <i class="material-icons">library_books</i>
          <p>{{ __('Content') }}
            <b class="caret"></b>
          </p>
        </a>
        <div class="collapse show" id="laravelExample">
          <ul class="nav">
            <li class="nav-item{{ $activePage == 'news' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('content.news') }}">
                <span class="sidebar-mini"> NEWS </span>
                <span class="sidebar-normal">{{ __('มาตรการเยียวยา') }} </span>
              </a>
            </li>
            <li class="nav-item{{ $activePage == 'live' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('content.live') }}">
                <span class="sidebar-mini"> LIVE </span>
                <span class="sidebar-normal"> {{ __('ศูนย์ข่าวโควิด ทำเนียบรัฐบาล') }} </span>
              </a>
            </li>
            <li class="nav-item{{ $activePage == 'faq' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('content.faq') }}">
                <span class="sidebar-mini"> FAQ </span>
                <span class="sidebar-normal"> {{ __('ถามตรง “เราไม่ทิ้งกัน”') }} </span>
              </a>
            </li>
            <li class="nav-item{{ $activePage == 'vdo' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('content.vdo') }}">
                <span class="sidebar-mini"> VDO </span>
                <span class="sidebar-normal"> {{ __('Clip') }} </span>
              </a>
            </li>
            <li class="nav-item{{ $activePage == 'banner' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('content.banner') }}">
                <span class="sidebar-mini"> BN </span>
                <span class="sidebar-normal"> {{ __('Banner') }} </span>
              </a>
            </li>
          </ul>
        </div>
      </li>
    </ul>
  </div>
</div>
