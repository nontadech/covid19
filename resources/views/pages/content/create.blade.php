@extends('layouts.app', ['activePage' => 'content', 'titlePage' => __('Content')])

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <form method="post" action="{{ route('content.store') }}"  enctype="multipart/form-data" autocomplete="off" class="form-horizontal">
                @csrf
                <input name="categories_id" type="hidden" value="{{ $categories_id }}" />
                <div class="card ">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title">{{ __('Add Content') }}</h4>
                    </div>
                    <div class="card-body ">
                        @if(in_array($categories_id, [1, 2]))
                        <div class="row">
                            <label class="col-sm-2 col-form-label">{{ __('Title') }}</label>
                            <div class="col-sm-10">
                            <div class="form-group{{ $errors->has('title') ? ' has-danger' : '' }}">
                                <input class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" value="{{ old('title') }}" name="title" id="input-title" type="text" placeholder="{{ __('Title') }}"  required="true" aria-required="true"/>
                                @if ($errors->has('title'))
                                <span id="name-error" class="error text-danger" for="input-title">{{ $errors->first('title') }}</span>
                                @endif
                            </div>
                            </div>
                        </div>
                        @endif
                        @if(in_array($categories_id, [1]))
                        <div class="row">
                            <label class="col-sm-2 col-form-label">{{ __('Descrition') }}</label>
                            <div class="col-sm-10">
                                <div class="form-group{{ $errors->has('descrition') ? ' has-danger' : '' }}">
                                    <textarea name="descrition" id="editor">{!! old('descrition') !!}</textarea>
                                    @if ($errors->has('descrition'))
                                    <span id="descrition-error" class="error text-danger" for="input-descrition">{{ $errors->first('descrition') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        @endif
                        @if(in_array($categories_id, [1, 3, 4]))
                        <div class="row">
                            <label class="col-sm-2 col-form-label">{{ __('Picture') }}</label>
                            <div class="col-sm-10">
                                <input type="file" name="picture" class="form-control">
                                @if ($errors->has('picture'))
                                <span id="descrition-error" class="error text-danger" for="input-picture">{{ $errors->first('picture') }}</span>
                                @endif
                            </div>
                        </div>
                        @endif
                        @if(in_array($categories_id, [4]))
                        <div class="row">
                            <label class="col-sm-2 col-form-label">{{ __('vdo') }}</label>
                            <div class="col-sm-10">
                                <input type="file" name="vdo" class="form-control">
                                @if ($errors->has('vdo'))
                                <span id="descrition-error" class="error text-danger" for="input-vdo">{{ $errors->first('vdo') }}</span>
                                @endif
                            </div>
                        </div>
                        @endif
                    </div>
                    <div class="card-footer ml-auto mr-auto">
                    <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    ClassicEditor
    .create( document.querySelector( '#editor' ) )
    .catch( error => {
        console.log( error );
    } );

</script>
<style>
.ck-editor__editable {
    min-height: 300px;
    width: 100%;
}
</style>
@endsection
