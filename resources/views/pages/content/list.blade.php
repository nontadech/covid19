@extends('layouts.app', ['activePage' => 'content', 'titlePage' => __('Content')])

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title ">Table Content <a href="{{ route('content.create', ['categories_id' => $categories_id]) }}"><span class="material-icons float-right">add_circle_outline</span></a></h4>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <th>
                    ID
                  </th>
                  <th>
                    @if(in_array($categories_id, [1]))
                      Title
                    @endif
                    @if(in_array($categories_id, [3,4]))
                      Picture
                    @endif
                  </th>
                  <th>
                    Action
                  </th>
                </thead>
                <tbody>
                @foreach ($article_list as $article)
                  <tr>
                    <td>
                      {{ $article->id }}
                    </td>
                    <td>
                      @if(in_array($categories_id, [1]))
                        {{ $article->title }}
                      @endif
                      @if(in_array($categories_id, [3,4]) && $article['file'])
                        <img src="{{ $article['file']['path'] }}" width="300">
                      @endif
                    </td>
                    <td>
                      <a href="{{ route('content.edit', ['content' => $article->id, 'categories_id' => $categories_id]) }}"><span class="material-icons">edit</span></a>
                      <a data-method="delete" class="jquery-postback" href="{{ route('content.destroy', ['content' => $article->id, 'categories_id' => $categories_id]) }}"><span class="material-icons">delete</span></a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="http://code.jquery.com/jquery-3.3.1.min.js"></script>
<script>

$(document).on('click', 'a.jquery-postback', function(e) {

    e.preventDefault(); // does not go through with the link.

    var $this = $(this);

    $.post({
        type: $this.data('method'),
        url: $this.attr('href'),
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    }).done(function (data) {
        alert('success');
        window.location.reload(false); 
    });
});
</script>
@endsection