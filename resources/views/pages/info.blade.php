@extends('layouts.master')
@section('title', 'Info')
@section('content')
<div class="container mt-5 mb-5">
    <img class="w-100" src="{{ asset($url)}}?2"/>
    @if($url_1)
    <img class="w-100 mt-5" src="{{ asset($url_1)}}"/>
    @endif
</div>
@stop   