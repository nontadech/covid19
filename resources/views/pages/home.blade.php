@extends('layouts.master')
@section('title', 'Home')
@section('content')
<div id="header-img" class="container-fluid position-relative">
    <img class="opacity-img-0 isWeb" src="{{ asset('assets/images/Teaser-banner-pc_e01.jpg?1')}}">
    <img class="opacity-img-0 isMobile" src="{{ asset('assets/images/Teaser-banner-mobile_e01.jpg')}}">
</div>
<div id="sesion-update" class="container">
    <div class="row">
        <div class="col-lg-8">
            <h4 class="text-center">Update : มาตรการเยียวยา <a href="{{ url('info/list')}}"><nobr>อ่านเพิ่มเติม</nobr></a></h4>
            <div class="gallery">
                <div class="gallery-container">
                    @foreach ($article_update as $article)
                    <div class="gallery-item text-center">
                        <div class="bg-light w-100">
                            <a href="{{ url('info/'.$article['id'])}}"><img class="w-100" src="{{ $article['file']['path'] }}"/></a>
                            <p>{{ $article['title'] }}</p>
                            <a href="{{ url('info/'.$article['id'])}}">รายละเอียด</a>
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="gallery-controls"></div>
            </div>
        </div>
        <div class="col-lg-4" id="live">
            <h5 class="live text-center">ศูนย์ข่าวโควิด ทำเนียบรัฐบาล</h5>
            {!! $article_live['descrition'] !!}
            <strong>{{ $article_live['title'] }}</strong>
        </div>
    </div>
 </div>
<div id="bg-news" class="container">
    <div class="row">
        <div class="col-12">
        <h3 class="icon-news">ข่าว : เกาะติดมาตรการ / เศรษฐกิจ </h3>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <ul class="news">
                <li class="active"><a data-cat="all">ทั้งหมด</a></li>
                <!--
                <li><a data-cat="all">มาตราการรัฐ</a></li>
                <li><a data-cat="all">นโยบาย</a></li>
                -->
                <li><a data-cat="money">เศรษฐกิจ</a></li>
                <li><a data-cat="finance">การเงิน/คลัง</a></li>
            </ul>
        </div>
    </div>
    <div class="row justify-content-center">
        <div id="slide-news" class="col-11 mt-3 pl-3">
            <div class="owl-carousel owl-carousel-news owl-theme">
                @foreach($content as $value)
                <div class="item">
                    <a target="_blank" title="{{ html_entity_decode($value['topic']) }}" href="{{ $value['link'] }}">
                        <div class="bg-light">
                            <img src="{{ $value['cover'] }}">
                            <p>{{ html_entity_decode($value['topic']) }}</p>
                            <label>{{ $value['category'] }}</label>
                        </div>
                    </a>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
<div id="faq" class="container">
    <div class="row">
        <div class="col-lg-5">
            <h3 class="icon-faq">ถามตรง “เราไม่ทิ้งกัน”</h3>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-11 mt-3 pl-3">
            <div class="owl-carousel owl-carousel-faq owl-theme">
                @foreach ($article_faq as $article)
                <div class="item">
                    <img class="w-100" src="{{ asset($article['file']['path'])}}">
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
<div id="vdo" class="container-fluid mt-4 pt-3 pb-3">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="mb-3">Clip VDO</h2>
            </div>
            <div class="col-12">
                <div class="owl-carousel owl-carousel-vdo owl-theme">
                @foreach ($article_vdo as $article)
                    <div class="item">
                        <video class="w-100" controls poster="{{ asset($article['picture']['path'])}}">
                            <source src="{{ asset($article['vdo']['path'])}}" type="video/mp4">
                            Your browser does not support the video tag.
                        </video>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
<div id="covid19" class="container mt-4 mb-3">
    <a target="_blank" href="{{ $article_banner['file']['link'] }}"><img class="w-100" src="{{ $article_banner['file']['path'] }}"></a>
</div> 
<script src="/js/carousel.js"></script>
<div id="modal-image" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
  <div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
        </div>
        <div class="modal-body">
        ...
        </div>
    </div>
  </div>
</div>

@stop   
