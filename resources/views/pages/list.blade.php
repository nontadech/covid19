@extends('layouts.master')
@section('title', 'Info')
@section('content')
<div class="container" id="info-list">
    <h1 class="mt-5">Update : มาตรการเยียวยา</h1>
    <div class="row mb-5">
        @foreach($info as $key => $value)
        <div class="col-lg-3 col-6 col-md-4 mt-5">
            <a href="{{ url('info/'.($key+1))}}">
                <div class="box">
                    <div class="image">
                        <img class="w-100" src="{{ asset($value['url'])}}?2"/>
                    </div>
                    <p>{{$value['detail']}}</p>
                </div>
            </a>
        </div>
        @endforeach
    </div>
</div>
@stop   