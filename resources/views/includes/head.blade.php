<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>@yield('title')</title>
<!-- plugins:css -->
<link rel="stylesheet" href="/css/app.css?18">
<link rel="stylesheet" href="/css/carousel.css?1">

<link rel="stylesheet" href="/css/owl.carousel.min.css">
<link rel="stylesheet" href="/css/owl.theme.default.min.css">
<!-- endinject -->
<script src="/js/lazyload.min.js"></script>
<script src="/js/app.js?10"></script>
<script src="/js/owl.carousel.min.js"></script>
<!-- Fonts -->
<link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

<!-- Styles -->