<div class="container-fluid bg-while mb-5 pt-3 pb-3">
    <div class="container">
        <div class="row text-center">
        <div class="col-md-6 offset-md-3">
            <img src="{{ asset('assets/images/footer/job-the-comptroller-general-is-department.png')}}" style="width: 10%;">
            <img src="{{ asset('assets/images/footer/FPO.png')}}" style="width: 10%;">
            <img src="{{ asset('assets/images/footer/MOF.png')}}" style="width: 10%;">
            <img src="{{ asset('assets/images/footer/Seal_of_the_Office_of_the_Prime_Minister_of_Thailand.png')}}" style="width: 10%;">
            <img src="{{ asset('assets/images/footer/Krungthai.png')}}" style="width: 10%;">
            <img src="{{ asset('assets/images/footer/bank-t.jpg')}}" style="width: 10%;">
            <img src="{{ asset('assets/images/footer/aomsin.png')}}" style="width: 10%;">
        </div>
        </div>
    </div>
</div>
<div class="container-fluid detail-container">
    <div class="row mx-3">
    <div class="col-md-3 col-sm-12 text-center">
        <img src="{{ asset('assets/images/logo.png')}}" style="width: 60px;">
    </div>
    <div class="col-md-8 col-sm-12">
        <div class="row pt-3">
        <div class="col-sm-12 col-md-6">
            <p class="title-header">สอบถามข้อมูลมาตรการ ฯ</p>
            <p>สำนักงานเศรษฐกิจการคลัง<br>โทร. 02 273 9020 ต่อ 3421, 3422, 3423, 3424, <br class="d-none d-lg-block">
            3425, 3427, 3429, 3430 และ 3572
            <br class="d-none d-lg-block"> (ในวันและเวลาราชการ)</p>
        </div>
        <div class="col-sm-12 col-md-6">
            <p class="title-header">สอบถามวิธีการลงทะเบียน สถานะการโอนเงิน</p>
            <p>โทร. 02 111 1144</p>
        </div>
        </div>
    </div>
    </div>
</div>