
<div class="bg-head-1">
    <div class="container">
    <nav class="navbar navbar-expand-lg navbar-light">
        <a class="logo isMobile" href="https://www.xn--12cl1ck0bl6hdu9iyb9bp.com/"><img  width="35" src="{{ asset('assets/images/logo.png')}}"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <!--
        <div class="font-size isWeb">ขนาดอักษร <span class="small">ก</span> <span class="big">ก</span></div>
        -->
        <div class="social-share">
            <a target="_blank" href="https://www.facebook.com/%E0%B9%80%E0%B8%A3%E0%B8%B2%E0%B9%84%E0%B8%A1%E0%B9%88%E0%B8%97%E0%B8%B4%E0%B9%89%E0%B8%87%E0%B8%81%E0%B8%B1%E0%B8%99-111685333826745/"><i class="fab fa-facebook-f"></i></a>
            <a target="_blank" href="https://twitter.com/raomaithingkun"><i class="fab fa-twitter"></i></a>
            <a target="_blank" href="https://www.instagram.com/raomaithingkun/?r=nametag"><i class="fab fa-instagram"></i></a>
            <!--<i class="fab fa-line"></i>-->
        </div>
    
        <div class="navbar-nav mr-auto isWeb">
        <!--
            <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="search" placeholder="ค้นหาที่นี้" aria-label="Search"><i class="fas fa-search"></i>
            </form>
            -->
        </div>
        <ul class="navbar-nav">
            <li class="nav-item active isMobile">
            <a class="nav-link" href="https://www.xn--12cl1ck0bl6hdu9iyb9bp.com/">หน้าแรก</a>
            </li>
               <!--
            <li class="nav-item dropdown isMobile">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                ข่าวสาร & ประชาสัมพันธ์
            </a>
         
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="#">1</a>
                <a class="dropdown-item" href="#">2</a>
            </div>
            </li>

            <li class="nav-item dropdown isMobile">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                เว็บบอร์ด
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="#">3</a>
                <a class="dropdown-item" href="#">4</a>
            </div>
            </li>
            <li class="nav-item dropdown isMobile">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                ติดต่อเรา
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="#">5</a>
                <a class="dropdown-item" href="#">6</a>
            </div>
            </li>
            -->
            <li class="nav-item isMobile">
                <a class="nav-link scrollLink" href="#sesion-update">มาตรการเยียวยา</a>
            </li>
            <li class="nav-item isMobile">
                <a class="nav-link scrollLink" href="#bg-news">ข่าว : เกาะติดมาตรการ</a>
            </li>
            <li class="nav-item isMobile">
                <a class="nav-link scrollLink" href="#vdo">Clip VDO</a>
            </li>
            <li class="nav-item">
            <a class="nav-link scrollLink" href="#covid19">ศูนย์ข่าวโควิด-19</a>
            </li>
            <li class="nav-item">
            <a class="nav-link scrollLink" href="#faq">ถามตรง “เราไม่ทิ้งกัน”</a>
            </li>
            <!--
            <li class="nav-item dropdown isWeb">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <img width="25" src="{{ asset('assets/images/flag_th.png')}}"> TH
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="#"><img width="25" src="{{ asset('assets/images/flag_th.png')}}"> TH</a>
                <a class="dropdown-item" href="#"><img width="25" src="{{ asset('assets/images/flag_uk.png')}}"> EN</a>
            </div>
            </li>
            -->
        </ul>
        </div>
    </nav>
    </div>
</div>
<div class="bg-head-2 isWeb">
    <div class="container position-relative">
    <a href="https://www.xn--12cl1ck0bl6hdu9iyb9bp.com/"><img class="logo" width="60" src="{{ asset('assets/images/logo.png')}}"></a>
    <nav class="d-none d-lg-flex navbar navbar-expand-lg navbar-light justify-content-end">
        <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="https://www.xn--12cl1ck0bl6hdu9iyb9bp.com/">หน้าแรก</a>
            </li>
            <li class="nav-item">
                <a class="nav-link scrollLink" href="#sesion-update">มาตรการเยียวยา</a>
            </li>
            <li class="nav-item">
                <a class="nav-link scrollLink" href="#bg-news">ข่าว : เกาะติดมาตรการ</a>
            </li>
            <li class="nav-item">
                <a class="nav-link scrollLink" href="#vdo">Clip VDO</a>
            </li>
            
                <!--
            <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                ข่าวสาร & ประชาสัมพันธ์
            </a>
        
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="#">1</a>
                <a class="dropdown-item" href="#">2</a>
            </div>
            </li>
            <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                เว็บบอร์ด
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="#">3</a>
                <a class="dropdown-item" href="#">4</a>
            </div>
            </li>
            <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                ติดต่อเรา
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="#">5</a>
                <a class="dropdown-item" href="#">6</a>
            </div>
            </li>
            -->
        </ul>
    </nav>
    </div>
</div>