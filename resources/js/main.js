$(function(){
    $('.owl-carousel-news').owlCarousel({
        margin:10,
        responsiveClass:true,
        nav:true,
        dots: false,
        lazyLoad:true,
        responsive:{
            0:{
                items:1,
            },
            600:{
                items:3,
            },
            1000:{
                items:6,
                loop:false
            }
        }
    })
    $('.owl-carousel-faq').owlCarousel({
        margin:10,
        nav: true,
        dots: false,
        lazyLoad: true,
        responsive:{
            0:{
                items:1,
            },
            600:{
                items:3,
            },
            1000:{
                items:4,
                loop:false
            }
        }
    })
   $('.owl-carousel-vdo').owlCarousel({
        margin:10,
        nav:true,
        dots: false,
        autoplay:true,
        loop:true,
        autoplayTimeout:5000,
        responsive:{
            0:{
                items:1,
            },
            600:{
                items:3,
            },
            1000:{
                items:4,
            }
        }
    }) 
    var owl =  $('.owl-carousel-update').owlCarousel({
        margin:10,
        nav:true,
        dots: false,
        autoplay:true,
        loop:true,
        autoplayTimeout:5000,
        responsive:{
            0:{
                items:1,
            },
            600:{
                items:3,
            }
        }
    })
    owl.on('initialize.owl.carousel', function() {
        console.log(1);
        $('.owl-carousel-update .owl-item.active').removeClass('animate');
        $('.owl-carousel-update .owl-item.active').eq(1).addClass('animate');
    })
    owl.on('translated.owl.carousel', function() {
        $('.owl-carousel-update .owl-item.active').removeClass('animate');
        $('.owl-carousel-update .owl-item.active').eq(1).addClass('animate');
    })
    $(".news li").click(function(){
        var cat = $(this).find('a').data('cat');
        $('.news li').removeClass('active');
        $(this).addClass('active');
        $.ajax({
            url: "/api/getContent/"+cat,
        }).done(function(data) {
           if(data.length){
            var html = '<div class="owl-carousel owl-carousel-news owl-theme">';
            for(var i = 0; i < data.length; i++){
                html += '<div class="item">';
                html += ' <a target="_blank" title="'+escapeHtml(data[i].topic)+'" href="'+data[i].link+'">';
                html += '   <div class="bg-light">';
                html += '       <img src="'+data[i].cover+'">';
                html += '       <p>'+escapeHtml(data[i].topic)+'</p>';
                html += '       <label>'+data[i].category+'</label>';
                html += '   </div>';
                html += ' </a>';
                html += ' </div>';
             
            }
            html += '</div>';
            $("#slide-news .owl-carousel-news").remove();
            $("#slide-news").html(html);
            $('.owl-carousel-news').owlCarousel({
                margin:10,
                responsiveClass:true,
                nav:true,
                dots: false,
                responsive:{
                    0:{
                        items:1,
                    },
                    600:{
                        items:3,
                    },
                    1000:{
                        items:6,
                        loop:false
                    }
                }
            })
           }
      
        });
    });
    new LazyLoad({
        elements_selector: ".lazy"
    });
    $(".owl-carousel-faq img").click(function(){
        $("#modal-image .modal-body").html("<img class='w-100' src='"+$(this).attr("src")+"'/>");
        $("#modal-image").modal();
    });
    galleryAuto();
    $( ".gallery-controls-next" ).click(function(){
        clearTimeout(time);
    });
    $( ".gallery-controls-previous" ).click(function(){
        clearTimeout(time);
    });
    $( ".gallery" ).mouseenter(function(){
        clearTimeout(time);
    });
    $( ".gallery" ).mouseleave(function(){
        galleryAuto();
    });
    $( "a.scrollLink" ).click(function( event ) {
        event.preventDefault();
        $("html, body").animate({ scrollTop: $($(this).attr("href")).offset().top }, 500);
    });
});
var time = 0;
function galleryAuto(){
    time = setTimeout(function name() {
        $( ".gallery-controls-next" ).trigger( "click" );
        galleryAuto()
    },3000)
}
function escapeHtml(text) {
    var map = {
      '&': '&amp;',
      '<': '&lt;',
      '>': '&gt;',
      '"': '&quot;',
      "'": '&#039;'
    };
  
    return text.replace(/[&<>"']/g, function(m) { return map[m]; });
  }