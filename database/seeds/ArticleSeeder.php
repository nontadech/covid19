<?php

use Illuminate\Database\Seeder;

class ArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('article')->insert([
            'categories_id' => 1,
            'users_id' => 1,
            'title' => 'ใครบ้าง หมดสิทธิ์รับ 5,000 บาท เยียวยาโควิด-19',
            'descrition' => '<img class="w-100" src="/assets/images/info_1_detail.jpg">',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('article')->insert([
            'categories_id' => 1,
            'users_id' => 1,
            'title' => '4 อาชีพ เฮ ล็อตแรก 5,000 บาท เยียวยาโควิด-19',
            'descrition' => '<img class="w-100" src="/assets/images/info_2_detail.jpg">',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('article')->insert([
            'categories_id' => 1,
            'users_id' => 1,
            'title' => 'กลุ่มเป้าหมายรับสิทธิ์เยียวยา 5,000 บาท (3 เดือนมาจากไหน)',
            'descrition' => '<img class="w-100" src="/assets/images/info_3_detail.jpg">',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('article')->insert([
            'categories_id' => 1,
            'users_id' => 1,
            'title' => 'เราไม่ทิ้งกัน ล็อตแรก 5,000 บาท ถึง 1.4 ล้านคน!',
            'descrition' => '<img class="w-100" src="/assets/images/info_4_detail.jpg"><img class="w-100 mt-5" src="/assets/images/info_4-1_detail.jpg">',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('article')->insert([
            'categories_id' => 1,
            'users_id' => 1,
            'title' => 'ทำไมไม่ได้ 5,000 จัดอยู่กลุ่ม เกษตรกร',
            'descrition' => '<img class="w-100" src="/assets/images/info_5_detail.jpg">',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('article')->insert([
            'categories_id' => 1,
            'users_id' => 1,
            'title' => "ร้องเรียนอย่าไปคลัง ไม่ได้ 5,000 ยื่น 'อุทธรณ์' เฉพาะ online เท่านั้น",
            'descrition' => '<img class="w-100" src="/assets/images/info_6_detail.jpg">',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('article')->insert([
            'categories_id' => 1,
            'users_id' => 1,
            'title' => 'เคลียร์ชัด? ได้สิทธิ์ 5,000 แต่เงินไม่เข้าบัญชี',
            'descrition' => '<img class="w-100" src="/assets/images/info_7_detail.jpg">',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('article')->insert([
            'categories_id' => 1,
            'users_id' => 1,
            'title' => 'คืบหน้าเยียวยา 5,000 บาท',
            'descrition' => '<img class="w-100" src="/assets/images/info_8_detail.jpg">',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('article')->insert([
            'categories_id' => 1,
            'users_id' => 1,
            'title' => 'เปิดเกณฑ์ ทบทวนสิทธิ์ เยี่ยวยา 5,000 บาท',
            'descrition' => '<img class="w-100" src="/assets/images/info_9_detail.jpg">',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('article')->insert([
            'categories_id' => 2,
            'users_id' => 1,
            'title' => '[Live] รัฐมนตรีว่าการกระทรวงการคลัง แถลงหลังการประชุม ครม.',
            'descrition' => '<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2FLiveNBT2HD%2Fvideos%2F673671673195574%2F&show_text=0&width=560" class="w-100 vdo-iframe"  scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('article')->insert([
            'categories_id' => 3,
            'users_id' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('article')->insert([
            'categories_id' => 3,
            'users_id' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('article')->insert([
            'categories_id' => 3,
            'users_id' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('article')->insert([
            'categories_id' => 3,
            'users_id' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('article')->insert([
            'categories_id' => 3,
            'users_id' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('article')->insert([
            'categories_id' => 4,
            'users_id' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('article')->insert([
            'categories_id' => 4,
            'users_id' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('article')->insert([
            'categories_id' => 4,
            'users_id' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('article')->insert([
            'categories_id' => 4,
            'users_id' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('article')->insert([
            'categories_id' => 4,
            'users_id' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('article')->insert([
            'categories_id' => 4,
            'users_id' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('article')->insert([
            'categories_id' => 4,
            'users_id' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('article')->insert([
            'categories_id' => 4,
            'users_id' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('article')->insert([
            'categories_id' => 4,
            'users_id' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('article')->insert([
            'categories_id' => 4,
            'users_id' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('article')->insert([
            'categories_id' => 4,
            'users_id' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('article')->insert([
            'categories_id' => 4,
            'users_id' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('article')->insert([
            'categories_id' => 4,
            'users_id' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('article')->insert([
            'categories_id' => 4,
            'users_id' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('article')->insert([
            'categories_id' => 4,
            'users_id' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('article')->insert([
            'categories_id' => 5,
            'users_id' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
