<?php

use Illuminate\Database\Seeder;

class FileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('file')->insert([
            'article_id' => 1,
            'type' => 'picture',
            'path' => '/assets/images/info_1_detail.jpg',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('file')->insert([
            'article_id' => 2,
            'type' => 'picture',
            'path' => '/assets/images/info_2_detail.jpg',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('file')->insert([
            'article_id' => 3,
            'type' => 'picture',
            'path' => '/assets/images/info_3_detail.jpg',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('file')->insert([
            'article_id' => 4,
            'type' => 'picture',
            'path' => '/assets/images/info_4_detail.jpg',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('file')->insert([
            'article_id' => 5,
            'type' => 'picture',
            'path' => '/assets/images/info_5_detail.jpg',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('file')->insert([
            'article_id' => 6,
            'type' => 'picture',
            'path' => '/assets/images/info_6_detail.jpg',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('file')->insert([
            'article_id' => 7,
            'type' => 'picture',
            'path' => '/assets/images/info_7_detail.jpg',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('file')->insert([
            'article_id' => 8,
            'type' => 'picture',
            'path' => '/assets/images/info_8_detail.jpg',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('file')->insert([
            'article_id' => 9,
            'type' => 'picture',
            'path' => '/assets/images/info_9_detail.jpg',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('file')->insert([
            'article_id' => 11,
            'type' => 'picture',
            'path' => '/assets/images/timeline_1.jpg',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('file')->insert([
            'article_id' => 12,
            'type' => 'picture',
            'path' => '/assets/images/timeline_2.jpg',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('file')->insert([
            'article_id' => 13,
            'type' => 'picture',
            'path' => '/assets/images/timeline_3.jpg',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('file')->insert([
            'article_id' => 14,
            'type' => 'picture',
            'path' => '/assets/images/timeline_4.jpg',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('file')->insert([
            'article_id' => 15,
            'type' => 'picture',
            'path' => '/assets/images/timeline_5.jpg',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('file')->insert([
            'article_id' => 16,
            'type' => 'vdo',
            'path' => '/assets/vdo/vdo-1.mp4',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('file')->insert([
            'article_id' => 17,
            'type' => 'vdo',
            'path' => '/assets/vdo/vdo-2.mp4',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('file')->insert([
            'article_id' => 18,
            'type' => 'vdo',
            'path' => '/assets/vdo/vdo-3.mp4',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('file')->insert([
            'article_id' => 19,
            'type' => 'vdo',
            'path' => '/assets/vdo/vdo-4.mp4',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('file')->insert([
            'article_id' => 20,
            'type' => 'vdo',
            'path' => '/assets/vdo/vdo-5.mp4',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('file')->insert([
            'article_id' => 21,
            'type' => 'vdo',
            'path' => '/assets/vdo/vdo-6.mp4',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('file')->insert([
            'article_id' => 22,
            'type' => 'vdo',
            'path' => '/assets/vdo/vdo-7.mp4',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('file')->insert([
            'article_id' => 23,
            'type' => 'vdo',
            'path' => '/assets/vdo/vdo-8.mp4',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('file')->insert([
            'article_id' => 24,
            'type' => 'vdo',
            'path' => '/assets/vdo/vdo-9.mp4',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('file')->insert([
            'article_id' => 25,
            'type' => 'vdo',
            'path' => '/assets/vdo/vdo-10.mp4',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('file')->insert([
            'article_id' => 26,
            'type' => 'vdo',
            'path' => '/assets/vdo/vdo-11.mp4',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('file')->insert([
            'article_id' => 27,
            'type' => 'vdo',
            'path' => '/assets/vdo/vdo-12.mp4',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('file')->insert([
            'article_id' => 28,
            'type' => 'vdo',
            'path' => '/assets/vdo/vdo-13.mp4',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('file')->insert([
            'article_id' => 29,
            'type' => 'vdo',
            'path' => '/assets/vdo/vdo-14.mp4',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('file')->insert([
            'article_id' => 30,
            'type' => 'vdo',
            'path' => '/assets/vdo/vdo-15.mp4',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('file')->insert([
            'article_id' => 16,
            'type' => 'picture',
            'path' => '/assets/images/vdo-1.jpg',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('file')->insert([
            'article_id' => 17,
            'type' => 'picture',
            'path' => '/assets/images/vdo-2.jpg',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('file')->insert([
            'article_id' => 18,
            'type' => 'picture',
            'path' => '/assets/images/vdo-3.jpg',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('file')->insert([
            'article_id' => 19,
            'type' => 'picture',
            'path' => '/assets/images/vdo-4.jpg',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('file')->insert([
            'article_id' => 20,
            'type' => 'picture',
            'path' => '/assets/images/vdo-5.jpg',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('file')->insert([
            'article_id' => 21,
            'type' => 'picture',
            'path' => '/assets/images/vdo-6.jpg',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('file')->insert([
            'article_id' => 22,
            'type' => 'picture',
            'path' => '/assets/images/vdo-7.jpg',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('file')->insert([
            'article_id' => 23,
            'type' => 'picture',
            'path' => '/assets/images/vdo-8.jpg',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('file')->insert([
            'article_id' => 24,
            'type' => 'picture',
            'path' => '/assets/images/vdo-9.jpg',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('file')->insert([
            'article_id' => 25,
            'type' => 'picture',
            'path' => '/assets/images/vdo-10.jpg',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('file')->insert([
            'article_id' => 26,
            'type' => 'picture',
            'path' => '/assets/images/vdo-11.jpg',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('file')->insert([
            'article_id' => 27,
            'type' => 'picture',
            'path' => '/assets/images/vdo-12.jpg',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('file')->insert([
            'article_id' => 28,
            'type' => 'picture',
            'path' => '/assets/images/vdo-13.jpg',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('file')->insert([
            'article_id' => 29,
            'type' => 'picture',
            'path' => '/assets/images/vdo-14.jpg',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('file')->insert([
            'article_id' => 30,
            'type' => 'picture',
            'path' => '/assets/images/vdo-15.jpg',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('file')->insert([
            'article_id' => 31,
            'type' => 'picture',
            'path' => '/assets/images/imgbanner04.jpg',
            'link' => 'https://www.thaigov.go.th/news/contents/details/26950',
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
